from os import stat
from os import walk
from os.path import join
from uuid import uuid4
from datetime import datetime
import json
from HashFile import HashFile


class Directory(object):
    """

    """
    name = str()
    files = list()
    sub_dirs = list()
    path = str()
    dir_stat = object()

    def get_files(self):
        return self.name

    def get_dirs(self):
        return self.files

    def get_dir_stat(self):
        return self.dir_stat

    def update(self, **kwargs):
        self.__dict__.update(kwargs)


class DirectoryMapper(object):
    """
        PyDocs here!
    """

    def __init__(self, root_dir=None):
        """

        :rtype : object
        TODO: update PyDocs
        """
        # Root dir to crawl from and list files/dirs
        self.root_dir = root_dir
        # Initializing self.files_by_dir as None
        self.files_by_dir = {}
        # directory dictionary
        self.directory_branches = {}
        # list of all files with full path
        self.all_files = []
        # Hasher
        self.hasher = HashFile()
        # execute list_files method and populate obj attributes
        self.map_directory(root_dir=self.root_dir)
        # walk through directory and return list of files
        self.all_files = self.traverse_directory(root_dir=self.root_dir)

    @staticmethod
    def traverse_directory(root_dir):
        """
        It receives a string as a root dir, the function traverses the directory.
        It returns a list with all files with full path name
        :param root:
        :return: dictionary
        :param root: str
        """

        def __dictify(ldirname, lsubdir, lfiles):
            dir_dict = {rdir: {'_id': str(uuid4()),
                               'full_path': ldirname,
                               'subdirs': lsubdir,
                               'files_list': lfiles}}
            return dir_dict

        # deprecated by __dictify
        #return [__dictify(ldirname=rdir, lsubdir=subdirs, lfiles=files)
        #        for rdir, subdirs, files in walk(root_dir)
        #        for f in files]
        #
        # Return a list comprehension fancy directory traverse
        # using os.walk. __dictify function returns the dictionary
        return [__dictify(ldirname=rdir, lsubdir=subdirs, lfiles=files)
                for rdir, subdirs, files in walk(root_dir) for f in files]

        # return [join(rdir, f) for rdir, subdir, files in walk(root_dir) for f in files]

    def map_directory(self, root_dir):
        """

        :type self: object
        """
        if self.root_dir is None:
            self.root_dir = root_dir
        # Resetting local attributes
        dir_list = []
        self.files_by_dir.clear()
        # create dict with files list
        try:
            # list comprehension for directory traversal
            # [[r, f] for r,s,f in walk(full_path)]
            # from os import walk
            # from os.path import join
            # [join(root,f) for root,dirs,files in walk(root) for f in files]
            for dirname, subdir, files in walk(root_dir):
                # creating local variables to feed stat key on dict
                (mode, ino, dev, nlink, uid, gid, size, atime, mtime, ctime) = stat(dirname)
                self.directory_branches[dirname] = {'_id': str(uuid4()),
                                                    'full_path': dirname,
                                                    'subdirectories': subdir,
                                                    'files': files,
                                                    'stat': dict(mode=mode, ino=ino, dev=dev, nlink=nlink, uid=uid, gid=gid, size=size, atime=atime, mtime=mtime, ctime=ctime),
                                                    'ctime': str(datetime.fromtimestamp(stat(dirname).st_ctime)),
                                                    'mtime': str(datetime.fromtimestamp(stat(dirname).st_mtime)),
                                                    'mode': stat(dirname).st_mode,
                                                    'file_hash': {}}
                # if there is files on dir execute md5sum on it
                if files:
                    for f in files:
                        self.directory_branches[dirname]['file_hash'].update({join(dirname, f): self.hasher.md5_sum(join(dirname, f))})
                        
                dir_list.append(dirname)

            for dir_name in dir_list:
                for dirname, subdir, files in walk(dir_name):
                    self.files_by_dir[dirname] = files
        except OSError as o:
            raise Exception('Error while accessing {0} \nError #{1}: {2}'.format(root_dir, o.errno, o.strerror))

    def get_files_by_dir(self):
        """

        """
        if self.files_by_dir:
            return self.files_by_dir
        else:
            self.map_directory(self.root_dir)
            # raise AttributeError("No files were listed")

    def count_files(self):
        """
        walk through files_by_dir list and count the number os items
        """
        counter = 0
        for k, v in self.files_by_dir.iteritems():
            counter += len(v)

        return counter

    def dict_to_json(self):
        return json.dumps(self.directory_branches, indent=4)

    def number_of_dirs(self):
        """

        :rtype : object
        """
        if self.directory_branches:
            try:
                return self.directory_branches.__len__()
            except:
                raise Exception('directory not mapped yet')

    def get_directory_tree(self):
        return self.directory_branches


if __name__ == '__main__':
    # Usage 1: Instantiate DirectoryMapper with full_path
    print("\n### USAGE 1 ##")
    tmp = DirectoryMapper(root_dir='/tmp/jboss/appServer')
    print('files by dir: {0}'.format(tmp.get_files_by_dir()))
    print('Total files: {0}'.format(tmp.count_files()))
    print('Files full path {0}'.format(tmp.all_files))
    print('all_files type: {0}'.format(type(tmp.all_files)))

    print('Directory Tree Branches: \n{0}'.format(tmp.get_directory_tree()))
    print('Indented tree: \n: {0}'.format(tmp.dict_to_json()))
    containers = []
    containers = {'container': [c for c in tmp.directory_branches['/tmp/jboss/appServer/server']['subdirectories']]}
    # <DirectoryMapperOBJ>.directory_branche[root_dir][stat] | [ctime] | [mtime] | [subdirectories]
    print([tmp.directory_branches[join('/tmp/jboss/appServer/server', c)] for c in tmp.directory_branches['/tmp/jboss/appServer/server']['subdirectories']])
    print(tmp.directory_branches['/tmp/jboss/appServer/server/201502271200-eldorado-chargeback-1.27']['ctime'])

    # Usage 2: Instantiate obj without full_path argument
    # print("\n### USAGE 2 ###")
    # home_dir = DirectoryMapper()
    #home_dir.list_all_files(full_path="~/")
    #print home_dir.get_files_by_dir()

    #print "ONE MORE"
    #home_dir.list_all_files(full_path="/tmp")
    #print home_dir.get_files_by_dir()
